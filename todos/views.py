from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse 
from .models import TodoList, TodoItem
from .forms import TodoListForm 


# Create your views here



def todo_list_list(request):
    todolists = TodoList.objects.all()
    
    return render(request, 'todos/todolist.html', {'todolists': todolists})


def todo_list_detail(request,id):
    todolist = get_object_or_404(TodoList, id=id)
    todo_items = todolist.items.all()
    return render(request, 'todos/todolist_detail.html', {'todolist': todolist, 'todo_items': todo_items})
    

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', id=todolist.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todolist_form.html', {'form': form})

def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect(reverse('todo_list_detail', args=[todolist.id]))
    else:
        form = TodoListForm(instance=todolist)
    return render(request, 'todos/todolist_form.html', {'form': form})