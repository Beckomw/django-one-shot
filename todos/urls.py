from django.urls import path, include
from . import views



urlpatterns = [
    path('', views.todo_list_list, name='todo_list_list'),
    path('create/', views.todo_list_create, name='todo_list_create'),
    path('<int:id>/', views.todo_list_detail, name='todo_list_detail'),
    path('<int:id>/update/', views.todo_list_update, name='todo_list_update'),
]