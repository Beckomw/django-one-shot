from django.contrib import admin
from .models import TodoList, TodoItem




@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

TodoItemAdmin = admin.site.register(TodoItem)